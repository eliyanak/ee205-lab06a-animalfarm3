///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   09_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <random>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

static random_device randomDevice;
static mt19937_64 RNG( randomDevice() );
static bernoulli_distribution boolRNG( 0.5 );

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch (color) {
      case BLACK:    return string("Black"); break;
      case WHITE:    return string("White"); break;
      case RED:      return string("Red"); break;
      case SILVER:   return string("Silver"); break;
      case YELLOW:   return string("Yellow"); break;
      case BROWN:    return string("Brown"); break;
   }

   return string("Unknown");
};

const Gender Animal::getRandomGender() {
   if( boolRNG( RNG ) )
      return MALE;
   else
      return FEMALE;
}

const enum Color Animal::getRandomColor() {
   uniform_int_distribution<> colorRNG(BLACK, BROWN);
   int r = colorRNG( RNG );

   switch( r ) {
      case 0: return BLACK;   break;
      case 1: return WHITE;   break;
      case 2: return RED;     break;
      case 3: return SILVER;  break;
      case 4: return YELLOW;  break;
      case 5: return BROWN;   break;
   }

   return BLACK;
}

const bool Animal::getRandomBool() {
   return boolRNG( RNG );
}

const float Animal::getRandomWeight( const float from, const float to ){
   uniform_real_distribution<> floatRNG(from, to);
   float f = floatRNG( RNG );

   return f;
}

const string Animal::getRandomName() {
   uniform_int_distribution<> lengthRNG(4, 9);
   uniform_int_distribution<> uppercaseRNG('A', 'Z');
   uniform_int_distribution<> lowercaseRNG('a', 'z');
   
   int length = lengthRNG( RNG );
   char randomName[length];

   randomName[0] = uppercaseRNG( RNG );
   for (int i = 1; i < length; i++){
      randomName[i] = lowercaseRNG( RNG );
   }
   // last value in string must be 0, otherwise an incorrect character will be generated
   randomName[length] = 0;

   return randomName;
}

Animal::Animal() {
   cout << "." ;
}

Animal::~Animal() {
   cout << "x" ;
}

} // namespace animalfarm
