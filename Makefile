###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Eliya Nakamura <eliyanak@hawaii.edu>
# @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
# @date   09_Mar_2021
###############################################################################

all: main

main.o:  animal.hpp factory.hpp main.cpp
	g++ -c main.cpp

test.o:	animal.hpp factory.hpp test.cpp
	g++ -c test.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp

factory.o: animal.hpp cat.hpp dog.hpp nunu.hpp aku.hpp palila.hpp nene.hpp factory.hpp factory.cpp
	g++ -c factory.cpp

mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o factory.o
	g++ -o main main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o factory.o

test: test.cpp *.hpp test.o animal.o factory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o test test.o animal.o factory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o


clean:
	rm -f *.o main test
