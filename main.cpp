///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Eliya Nakamura <eliyanak@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   11_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "factory.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3" << endl;

   // fill the array
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for ( int i = 0; i < 25 ; i++ ) {
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   // print information about the array
   cout << endl << "Array of Animals" << endl;
   cout << "   Is it empty: " << animalArray.empty() << endl;
   cout << "   Number of elements: " << animalArray.size() << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;

   // iterate through the array
   for ( Animal* animal : animalArray ) {
      if (animal == NULL)
         continue;

      cout << animal->speak() << endl;
   }
   
   // empty array
   for ( int i = 0; i < animalArray.size(); i++ ) {
      delete animalArray[i];
   }

   // fill the list
   list<Animal*> animalList;
   for ( int i = 0; i < 25 ; i++ ) {
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }

   // print information about the list
   cout << endl << "List of Animals" << endl;
   cout << "   Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "   Number of elements: " << animalList.size() << endl;
   cout << "   Max size: " << animalList.max_size() << endl;

   // iterate through the list
   for ( Animal* animal : animalList ) {
      cout << animal->speak() << endl;
   }

   // empty list
   for ( Animal* animal : animalList ) {
      delete animal;
   }

   return 0;
}
